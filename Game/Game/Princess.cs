﻿using System;

namespace Game
{
    class Princess
    {
        public int X { get; private set; } = Wall.X - 1;
        public int Y { get; private set; } = Wall.Y - 1;
        public Point Position { get; private set; }
        public bool Open { get; private set; }

        public Princess()
        {
            Position = new Point(X, Y, '*');
            Console.ForegroundColor = ConsoleColor.Yellow;
            Position.Draw();
            Console.ResetColor();
        }

        public bool Find(Point p)
        {
			if (p == Position)
            {
                Open = true;
                return true;
            }

            return false;
        }
    }
}
