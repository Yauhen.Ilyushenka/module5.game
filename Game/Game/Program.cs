﻿using System;

namespace Game
{
    class Program
    {
        static readonly int X = 10;
        static readonly int Y = 10;
        const int bombsCount = 10;
        static Wall wall;
        static BombFactory bomb;
        static Hero hero;
        static Princess princess;

        public void Start()
        {
            Information.GetWelcomeOfGame();

            if (!Information.CloseApp)
            {
                while (true)
                {
                    Console.Clear();

                    wall = new Wall(new Point(X, Y, '#'));
                    bomb = new BombFactory(new Point(X, Y, ' '), bombsCount);
                    hero = new Hero(1, 1);
                    princess = new Princess();

                    Information.GetInformationAboutHero(hero.Health, bomb.Quantity);
                    hero.Move(wall, bomb, princess);

                    if (Information.GetOneMoreAttemp())
                    {
                        continue;
                    }

                    break;
                }
            }
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            program.Start();
        }
    }
}

