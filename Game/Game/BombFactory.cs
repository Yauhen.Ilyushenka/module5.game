﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game
{
    class BombFactory
    {
        private int x, y, countOfBombs;
        private char symbol;
        public int Quantity { get; private set; }
        public Point bomb { get; private set; }
        private List<Point> listOfBombs = new List<Point>();
        Random rnd = new Random();

        public BombFactory(Point point, int countOfBomb)
        {
            if (countOfBomb < 0 || countOfBomb > (((Wall.X - 1) * (Wall.Y - 1)) - 2))
            {
                throw new ArgumentOutOfRangeException(nameof(countOfBomb), $"Should set count of bomb from 0 to {((Wall.X - 1) * (Wall.Y - 1)) - 2}");
            }

            this.x = point.X;
            this.y = point.Y;
            this.symbol = point.Symbol;
            this.countOfBombs = countOfBomb;
            CreateBombs(this.countOfBombs);
        }

        public bool isExplosion(Point p)
        {
            for (int i = 0; i < listOfBombs.Count(); i++)
            {
                if (p == listOfBombs[i])
                {
                    listOfBombs.Remove(listOfBombs[i]);
                    Quantity--;
                    return true;
                }
            }
            return false;
        }

        private void CreateBombs(int countOfBomb)
        {
            while (countOfBomb != 0)
            {
                bomb = new Point(rnd.Next(1, x), rnd.Next(1, y), symbol);
                CheakPlace(bomb);
                listOfBombs.Add(bomb);
                Console.ForegroundColor = ConsoleColor.Red;
                bomb.Draw();
                Console.ResetColor();
                countOfBomb--;
                Quantity++;
            }
        }

        public int InflictDemage()
        {
            return rnd.Next(1, 10);
        }

        private Point CheakPlace(Point bomb)
        {
            if (listOfBombs.Count() != 1)
            {
                for (int i = 0; i < listOfBombs.Count(); i++)
                {
                    if (listOfBombs[i] == bomb)
                    {
                        bomb.X = rnd.Next(1, x);
                        bomb.Y = rnd.Next(1, y);
                    }
                }
            }
            return bomb;
        }
    }
}
