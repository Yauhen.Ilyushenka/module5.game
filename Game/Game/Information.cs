﻿using System;
using System.Linq;
using System.Threading;

namespace Game
{
    static class Information
    {
        public static bool CloseApp { get; private set; } = false;
        static int height = Console.WindowHeight / 2;
        static int widht = Console.WindowWidth / 2 - 7;

        public static void GetInformationAboutHero(int health, int countOfBomb, bool openPrincess = false)
        {

            Console.SetCursorPosition(Wall.X + 3, 0);
            Console.Write($"Health of Hero: {health} \tQuantity of Bombs: {countOfBomb} ");

            if (health <= 0)
            {
                Console.SetCursorPosition(Wall.X + 3, 2);
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Game Over!");
            }

            else if (openPrincess)
            {
                Console.SetCursorPosition(Wall.X + 3, 2);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Princess found! Mission complited!");
            }

            Console.ResetColor();

        }

        public static bool GetOneMoreAttemp()
        {
            while (true)
            {
                Console.SetCursorPosition(Wall.X + 3, 4);
                Console.Write("One more time? (Y/N)\t Response:");
                Console.CursorVisible = true;
                ConsoleKeyInfo key = Console.ReadKey();
                Console.CursorVisible = false;

                switch (key.Key)
                {
                    case ConsoleKey.Y:
                        return true;
                    case ConsoleKey.N:
                        return false;
                    default:
                        continue;
                }
            }
        }

        public static void GetWelcomeOfGame()
        {
            Console.SetCursorPosition(Console.WindowWidth / 2 - 10, Console.WindowHeight / 2 - 10);

            WriteTextWithBorder("Game Princess");
            Thread.Sleep(1000);
            WriteMenu();
            Thread.Sleep(1000);
            GetChoice();
        }

        private static void WriteTextWithBorder(string text)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            int height = Console.WindowHeight / 2 - 10;
            string newStr = text;

            for (int i = 0; i < text.Length; i++)
            {
                newStr = newStr.Replace(newStr[i], '-');
            }
            Console.WriteLine($"+-{newStr}-+");
            height++;
            Console.SetCursorPosition(Console.WindowWidth / 2 - 10, height);
            Console.WriteLine($"| {text} |");
            height++;
            Console.SetCursorPosition(Console.WindowWidth / 2 - 10, height);
            Console.WriteLine($"+-{newStr}-+");
            Console.CursorVisible = false;
            Console.ResetColor();
        }

        private static void WriteMenu()
        {
            var array = new string[] { "1. Start", "2. The rule", "3. Exit" };

            var listOfItem = array.Select(x => x).ToList();

            for (int i = 0; i < listOfItem.Count; i++)
            {
                Console.SetCursorPosition(widht, height);
                WriteText(listOfItem[i]);
                height += 2;
            }
        }

        private static void WriteText(string text) => Console.Write(text);

        private static void GetChoice()
        {
            while (true)
            {
                Console.SetCursorPosition(widht - 10, height + 3);
                Console.ForegroundColor = ConsoleColor.Cyan;
                WriteText("Please, pick the number:");
                Console.ResetColor();
                Console.CursorVisible = true;
                ConsoleKeyInfo key = Console.ReadKey();
                Console.CursorVisible = false;

                switch (key.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        Console.Clear();
                        Console.SetCursorPosition(Console.WindowWidth / 2 - 20, Console.WindowHeight / 2 - 5);
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        WriteText("Now, young Skywalker...you will die!");
                        Console.ResetColor();
                        Thread.Sleep(3000);
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        GetRule();
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        CloseApp = true;
                        break;
                    default:
                        continue;
                }
                break;
            }
        }

        private static void GetRule()
        {
            Console.Clear();

            string history = "You are on the same square with the princess and you need to help her. Be careful! Bombs are installed on the field.\n";
            string instructionToMove = "You should use to move such keys as:\n\nConsoleKey.UpArrow : move to up;\nConsoleKey.DownArrow : move to down;\nConsoleKey.RightArrow : move to right;\nConsoleKey.LeftArrow : move to left.\n";

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            WriteText(history);
            Console.ResetColor();
            Console.WriteLine();
            WriteText(" x - You\n * - Princess\n");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            WriteText(instructionToMove);
            Console.ResetColor();
            Console.WriteLine();
            WriteText("Good luck!\n");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            WriteText("If you are ready to start the game, press any key: ");
            Console.ResetColor();
            Console.CursorVisible = true;
            Console.ReadKey();
            Console.CursorVisible = false;
        }
    }
}
