﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game
{
    class Wall
    {
        private char symbol;

        public static int X { get; private set; }

        public static int Y { get; private set; }

        private List<Point> wall = new List<Point>();

        public Wall(Point point)
        {
            if (point.X <= 1 && point.Y <= 1)
            {
                throw new ArgumentOutOfRangeException("Value X > 1, Value Y > 1");
            }

            else if (point.X <= 1)
            {
                throw new ArgumentOutOfRangeException(nameof(point.X), $"Value: X > 1");
            }

            else if (point.Y <= 1)
            {
                throw new ArgumentOutOfRangeException(nameof(point.Y), $"Value: Y > 1");
            }

            X = point.X + 1;
            Y = point.Y + 1;
            this.symbol = point.Symbol;

            Console.ForegroundColor = ConsoleColor.DarkBlue;
            DrawHorizontalLine(X, 0);
            DrawVerticalLine(X, Y);
            DrawHorizontalLine(X, Y);
            DrawVerticalLine(0, Y);
            Console.ResetColor();
        }

        public bool IsHit(Point p)
        {
            return wall.Any(x => x == p);
        }

        private void DrawHorizontalLine(int x, int y)
        {
            for (int i = 0; i < x; i++)
            {
                Point point = new Point(i, y, symbol);
                point.Draw();
                wall.Add(point);
            }
        }

        private void DrawVerticalLine(int x, int y)
        {
            for (int i = 0; i < y; i++)
            {
                Point point = new Point(x, i, symbol);
                point.Draw();
                wall.Add(point);
            }
        }

    }
}
