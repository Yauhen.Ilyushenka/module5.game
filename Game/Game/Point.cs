﻿using System;

namespace Game
{
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public char Symbol { get; set; }

        public Point(int x, int y, char ch = 'x')
        {
            X = x;
            Y = y;
            Symbol = ch;
        }

        public void Draw() => DrawPoint(Symbol);

        public void Clear() => DrawPoint(' ');

        public static bool operator ==(Point a, Point b) => (a.X == b.X && a.Y == b.Y) ? true : false;

        public static bool operator !=(Point a, Point b) => (a.X != b.X || a.Y != b.Y) ? true : false;

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        private void DrawPoint(char symbol)
        {
            Console.SetCursorPosition(X, Y);
            Console.Write(symbol);
            Console.SetCursorPosition(X, Y);
        }

    }
}
