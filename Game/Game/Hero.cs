﻿using System;

namespace Game
{
    class Hero
    {
        public int Health { get; private set; } = 10;
        public int X { get; private set; }
        public int Y { get; private set; }
        public Point CurrentPosition { get; private set; }
        public Point LastPosition { get; private set; }
        public Direction direction { get; private set; }
        Random random = new Random();

        public Hero(int x, int y)
        {
            if ((x < 1 || x > Wall.X - 1) && (y < 1 || y > Wall.Y - 1))
            {
                throw new ArgumentOutOfRangeException($"Should set position of hero X from 1 to {Wall.X - 1} and Y from 1 to {Wall.Y - 1}");
            }

            else if (x < 1 || x > Wall.X - 1)
            {
                throw new ArgumentOutOfRangeException(nameof(x), $"Should set position of hero X from 1 to {Wall.X - 1}");
            }
            else if (y < 1 || y > Wall.Y - 1)
            {
                throw new ArgumentOutOfRangeException(nameof(y), $"Should set position of hero Y from 1 to {Wall.Y - 1}");
            }

            X = x;
            Y = y;
            CurrentPosition = new Point(X, Y);
            CurrentPosition.Draw();
        }

        public void Move(Wall wall, BombFactory bomb, Princess princess)
        {
            while (true)
            {
                Console.SetCursorPosition(X, Y);

                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.UpArrow:
                        direction = Direction.Up;
                        break;
                    case ConsoleKey.DownArrow:
                        direction = Direction.Down;
                        break;
                    case ConsoleKey.RightArrow:
                        direction = Direction.Right;
                        break;
                    case ConsoleKey.LeftArrow:
                        direction = Direction.Left;
                        break;
                    default:
                        CurrentPosition.Draw();
                        continue;
                }

                IsContact(wall, bomb, princess, direction);

                if (Health <= 0 || princess.Position == CurrentPosition)
                {
                    break;
                }
            }
        }

        private int GetDamage(BombFactory bomb)
        {
            return Health -= bomb.InflictDemage();
        }

        private void DoStep()
        {
            LastPosition.Clear();
            CurrentPosition.Draw();
        }

        private void GetBackStep()
        {
            CurrentPosition = LastPosition;

            switch (direction)
            {
                case Direction.Up:
                    Y++;
                    break;
                case Direction.Down:
                    Y--;
                    break;
                case Direction.Right:
                    X--;
                    break;
                case Direction.Left:
                    X++;
                    break;
            }

            Console.SetCursorPosition(X, Y);
        }

        private void IsContact(Wall wall, BombFactory bomb, Princess princess, Direction direction)
        {
            LastPosition = CurrentPosition;

            switch (direction)
            {
                case Direction.Up:
                    CurrentPosition = new Point(X, --Y);
                    break;
                case Direction.Down:
                    CurrentPosition = new Point(X, ++Y);
                    break;
                case Direction.Right:
                    CurrentPosition = new Point(++X, Y);
                    break;
                case Direction.Left:
                    CurrentPosition = new Point(--X, Y);
                    break;
            }

            if (wall.IsHit(CurrentPosition))
            {
                GetBackStep();
            }

            else if (bomb.isExplosion(CurrentPosition))
            {
                Health = GetDamage(bomb);
                Information.GetInformationAboutHero(Health, bomb.Quantity);
                DoStep();
            }

            else if (princess.Find(CurrentPosition))
            {
                DoStep();
                Information.GetInformationAboutHero(Health, bomb.Quantity, princess.Open);
            }

            DoStep();
        }
    }
}

